const express = require("express");
const router = express.Router();
const auth = require("../auth");
const productController = require("../controllers/productController");

// Route for creating/selling a product
router.post("/sell", auth.verify, (req, res) => {
    const sessionData = auth.decode(req.headers.authorization);
    productController.sellProduct(sessionData, req.body).then(result => res.send(result));
});

// Route for retrieving all active products.
router.get("/", (req, res) => {
    productController.viewAllActiveProducts().then(result => res.send(result));
});

// Route for retrieving all active products.
router.get("/all", (req, res) => {
    const sessionData = auth.decode(req.headers.authorization);
    productController.viewAllProducts(sessionData).then(result => res.send(result));
});

// Route for retrieving a single item
router.get("/:productId", (req, res) => {
    productController.viewProduct(req.params.productId).then(result => res.send(result));
});

// Route for updating a product.
router.put("/:productId", auth.verify, (req, res) => {
    const sessionData = auth.decode(req.headers.authorization);
    productController.updateProduct(req.params, req.body, sessionData).then(result => res.send(result));
});

router.patch("/:productId/archive", auth.verify, (req, res) => {
    const sessionData = auth.decode(req.headers.authorization);
    productController.archiveProduct(req.params, sessionData).then(result => res.send(result));
})

router.patch("/:productId/unarchive", auth.verify, (req, res) => {
    const sessionData = auth.decode(req.headers.authorization);
    productController.unarchiveProduct(req.params, sessionData).then(result => res.send(result));
})

module.exports = router;
